<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S03A: Activity Classes and Objects</title>
</head>

<body>

    <h1>Person</h1>
    <?php $person = new Person('Senku', '', 'Ishigami'); ?>
    <p> <?php echo $person->printName(); ?> </p>

    <h1>Developer</h1>
    <?php $developer = new Developer('John', 'Finch', 'Smith'); ?>
    <p> <?php echo $developer->printName(); ?> </p>

    <h1>Engineer</h1>
    <?php $engineer = new Engineer('Harold', 'Myers', 'Reese'); ?>
    <p> <?php echo $engineer->printName(); ?> </p>


</body>

</html>